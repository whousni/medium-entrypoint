const express = require('express');
const cors = require('cors');
const request = require('request');
const Feed = require('rss-to-json');

const app = express();
app.use(cors())

const username = 'w.housni22'
const MEDIUM_API = `https://medium.com/@${username}/latest?format=json`;
const MEDIUM_RSS = `https://medium.com/feed/@${username}`;

 
app.get("/posts", (req, res, next) => {
    request.get(MEDIUM_API, (err, SuccessApi, body) => {
      if (!err && SuccessApi.statusCode === 200) {
        let i = body.indexOf("{");
        const { payload } = JSON.parse(body.substr(i));
        const { references } = payload;
        const { User, Post } = references;

        res.json({ users: Object.values(User), posts: Object.values(Post) });
      } else {
        res.sendStatus(500).json(err);
      }
    })
  });

const port = process.env.PORT || 4000;
app.listen(port,() => {
    console.log(`server is runnin on port ${port}`)
});